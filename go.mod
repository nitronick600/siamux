module gitlab.com/nitronick600/siamux

go 1.13

require (
	gitlab.com/NebulousLabs/errors v0.0.0-20171229012116-7ead97ef90b8
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/nitronick600/siacorn v0.0.0-20200219050137-b02354961505
	golang.org/x/crypto v0.0.0-20200208060501-ecb85df21340
)
