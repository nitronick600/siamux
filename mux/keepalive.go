package mux

import "time"

// staticUpdateDeadline handles extending the timeout of the mux's underlying
// connection whenever a frame was read or written.
func (m *Mux) staticUpdateDeadline(maxTimeout uint16) {
	duration := time.Second * time.Duration(maxTimeout)
	m.staticConn.SetDeadline(time.Now().Add(duration))
}
