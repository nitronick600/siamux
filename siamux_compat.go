package siamux

import (
	"math"
	"net"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/nitronick600/siacorn/persist"
	"gitlab.com/nitronick600/siamux/mux"
)

// CompatV1421NewWithKeyPair is like New but will use the provided keys instead
// of generated or existing ones. For safety this should only be called the
// first time after upgrading a node since it will always use the provided keys
// instead of the existing ones.
func CompatV1421NewWithKeyPair(address string, log *persist.Logger, persistDir string, privKey mux.ED25519SecretKey, pubKey mux.ED25519PublicKey) (*SiaMux, error) {
	// Listen on the specified address.
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return nil, err
	}
	// Create a client since it's the same as creating a server.
	sm := &SiaMux{
		staticAppSeed:    appSeed(fastrand.Uint64n(math.MaxUint64)),
		staticLog:        log,
		staticListener:   listener,
		handlers:         make(map[string]Handler),
		muxs:             make(map[appSeed]*mux.Mux),
		outgoingMuxs:     make(map[string]*mux.Mux),
		muxSet:           make(map[*mux.Mux]muxInfo),
		staticPersistDir: persistDir,
		stopped:          make(chan struct{}),
	}
	// Init the persistence.
	if err := sm.initPersist(); err != nil {
		return nil, errors.AddContext(err, "failed to initialize SiaMux persistence")
	}
	// Overwrite the keys.
	sm.staticPubKey = pubKey
	sm.staticPrivKey = privKey
	if err := sm.savePersist(); err != nil {
		return nil, errors.AddContext(err, "failed to override keys")
	}
	// Spawn the listening thread.
	sm.wg.Add(1)
	go func() {
		sm.threadedListen(listener)
		sm.wg.Done()
	}()
	return sm, nil
}
