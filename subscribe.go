package siamux

// This file contains code relevant to the two handshakes executed by the
// SiaMux.
// 1. AppSeed handshake
// This handshake happens right after a mux.Mux is created. The first stream
// that is created using the new mux will send the own appSeed and the receiver
// will return its own appSeed. Those two are added together to form a unique
// seed which can be used to deduplicate connections between the same
// applications.
// 2. Subscriber handshake
// The subscriber handshake happens every time a new stream is created. When
// creating a stream the user sends a subscriber name to subscribe to a
// listener. Upon success the listening SiaMux will return a response with an
// empty string and otherwise an error message if the specified listener doesn't
// exist.

import (
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/nitronick600/siacorn/encoding"
)

var errUnknownSubscriber = errors.New("unknown subscriber")

const (
	encodingMaxLen = 4096
)

type (
	// appSeed is a random number, which is not persisted, to uniquely identify an
	// application using a SiaMux in case multiple muxes run on the same
	// machine.
	appSeed uint64
	// subscriberRequest is the request sent at the beginning of every new
	// stream to let the other SiaMux know which listener the sender wants to
	// subscribe to.
	subscriberRequest struct {
		Subscriber string
	}
	// subscriberResponse is the response of the receiver to receiving a
	// subscriberRequest. It will return an empty string if subscribing was
	// successful and an error otherwise.
	subscriberResponse struct {
		Err string
	}
	// seedRequest is the request sent by the SiaMux upon connecting to another
	// SiaMux. After setting up the mux.Mux a stream is created to send the
	// request, receive the response and then closed.
	seedRequest struct {
		AppSeed appSeed
	}
	// seedResponse is the response sent by the SiaMux upon being connected to
	// by another SiaMux. After setting up the mux.Mux a stream is created to
	// send the request, receive the response and then closed.
	seedResponse struct {
		AppSeed appSeed
	}
)

func writeSubscriberRequest(stream Stream, subscriber string) error {
	return encoding.WriteObject(stream, subscriberRequest{
		Subscriber: subscriber,
	})
}

func writeSubscriberResponse(stream Stream, err error) error {
	var errStr string
	if err != nil {
		errStr = err.Error()
	}
	return encoding.WriteObject(stream, subscriberResponse{
		Err: errStr,
	})
}

func readSubscriberRequest(stream Stream) (string, error) {
	var sr subscriberRequest
	if err := encoding.ReadObject(stream, &sr, encodingMaxLen); err != nil {
		return "", errors.AddContext(err, "readSubsriberRequest: failed to ReadObject")
	}
	return sr.Subscriber, nil
}

func readSubscriberResponse(stream Stream) (error, error) {
	var sr subscriberResponse
	if err := encoding.ReadObject(stream, &sr, encodingMaxLen); err != nil {
		return nil, errors.AddContext(err, "readSubscriberResponse: failed to ReadObject")
	}
	if sr.Err == "" {
		return nil, nil
	}
	return errors.New(sr.Err), nil
}

func writeSeedRequest(stream Stream, appSeed appSeed) error {
	return encoding.WriteObject(stream, seedRequest{
		AppSeed: appSeed,
	})
}

func writeSeedResponse(stream Stream, appSeed appSeed) error {
	return encoding.WriteObject(stream, seedResponse{
		AppSeed: appSeed,
	})
}

func readSeedRequest(stream Stream) (appSeed, error) {
	var sr seedRequest
	if err := encoding.ReadObject(stream, &sr, encodingMaxLen); err != nil {
		return 0, errors.AddContext(err, "readSeedRequest: failed to ReadObject")
	}
	return sr.AppSeed, nil
}

func readSeedResponse(stream Stream) (appSeed, error) {
	var sr seedResponse
	if err := encoding.ReadObject(stream, &sr, encodingMaxLen); err != nil {
		return 0, errors.AddContext(err, "readSeedResponse: failed to ReadObject")
	}
	return sr.AppSeed, nil
}
